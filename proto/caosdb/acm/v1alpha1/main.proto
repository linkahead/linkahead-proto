//
// This file is a part of the CaosDB Project.
//
// Copyright (C) 2021-2022 Timm Fitschen <t.fitschen@indiscale.com>
// Copyright (C) 2021-2022 IndiScale GmbH <info@indiscale.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// This is the main file of the caosdb.acm.v1alpha1 package.
syntax                     = "proto3";
option java_multiple_files = true;
option java_package        = "org.caosdb.api.acm.v1alpha1";

package caosdb.acm.v1alpha1;

///////////////////////////////////////////
// COMMON MESSAGES
///////////////////////////////////////////

// Client preferences for the paging. The server MUST respect the index
// property or send the complete result set. The server MAY choose to send a
// different page length in the response
message PagingRequest {
  // Desired index of the first element in the response. The index depends on
  // the order of the elements in the result set.
  int32 index = 1;
  // Desired length of the page in the server response.
  int32 page_length = 2;
}

// The actual paging of the response.
message PagingResponse {
  // Total numbers of results.
  int32 total_number = 1;
  // Index of the first item in the page.
  int32 current_index = 2;
}

///////////////////////////////////////////
// PERMISSSIONS
///////////////////////////////////////////

// Request to list all known permissions (excluding entity permissions).
message ListKnownPermissionsRequest {}

// Response with all known permissions. Clients may choose to create drop-down
// menues from this list.
message ListKnownPermissionsResponse {
  // known permissions
  repeated PermissionDescription permissions = 1;
}

// Description of a permission.
message PermissionDescription {
  // The permission
  string permission = 1;
  // The description
  string description = 2;
}

// PermissionRule. These belong to Roles.
message PermissionRule {
  // The permission which is being granted oder denied.
  string permission = 1;
  // Priority permission rules (TRUE) overrule non-priority (FALSE) permission
  // rules.
  bool priority = 2;
  // True means that the permission is being granted by this rule, false means
  // the permission is being DENIED!
  bool grant = 3;
}

///////////////////////////////////////////
// ROLES
///////////////////////////////////////////

// Role
message Role {
  // Unique name of this role.
  string name = 1;
  // Description of the purpose of this role or which group of users this role
  // represents.
  string description = 2;
  // List of permission rules for this role.
  repeated PermissionRule permission_rules = 3;
}

// Request message for the ListRoles RPC
message ListRolesRequest {
  // Desired paging settings for the response.
  PagingRequest paging = 1;
}

// Response message for the ListRoles RPC
message ListRolesResponse {
  // Actual paging setting of the response.
  PagingResponse paging = 1;
  // A subset (when paging is used) or the complete set (otherwise) of roles.
  repeated ListRoleItem roles = 2;
}

// Combines role, the role's capabilities and the current users permissions.
message ListRoleItem {
  // Role
  Role role = 1;
  // The permissions of the user of the current session w.r.t. this role.
  repeated RolePermissions permissions = 2;
  // What can be done with this role.
  repeated RoleCapabilities capabilities = 3;
}

// Request message for the CreateSingleRole RPC
message CreateSingleRoleRequest {
  // The new role.
  Role role = 1;
}

// Response message for the CreateSingleRole RPC
message CreateSingleRoleResponse {}

// Request message for the RetrieveSingleRole RPC
message RetrieveSingleRoleRequest {
  // the name of the role.
  string name = 1;
}

// Response message for the RetrieveSingleRole RPC
message RetrieveSingleRoleResponse {
  // The role.
  Role role = 1;
  // Known users with this role
  repeated User users = 2;
  // The permissions of the user of the current session w.r.t. this role.
  repeated RolePermissions permissions = 3;
  // What can be do with this role.
  repeated RoleCapabilities capabilities = 4;
}

// Request message for the UpdateSingleRole RPC
message UpdateSingleRoleRequest {
  // The role
  Role role = 1;
}
// Response message for the UpdateSingleRole RPC
message UpdateSingleRoleResponse {}

// Request message for the DeleteSingleRole RPC
message DeleteSingleRoleRequest {
  // the name of the role.
  string name = 1;
}

// Response message for the DeleteSingleRole RPC
message DeleteSingleRoleResponse {}

// Role Permissions
enum RolePermissions {
  // Unspecified permission
  ROLE_PERMISSIONS_UNSPECIFIED = 0;
  // Role can be deleted
  ROLE_PERMISSIONS_DELETE = 1;
  // Description can be updated
  ROLE_PERMISSIONS_UPDATE_DESCRIPTION = 2;
  // Permission rules of this role can be updated
  ROLE_PERMISSIONS_UPDATE_PERMISSION_RULES = 3;
  // This role can be assigned
  ROLE_PERMISSIONS_ASSIGN = 4;
}

// Role Capabilities
enum RoleCapabilities {
  // Unspecified capability
  ROLE_CAPABILITIES_UNSPECIFIED = 0;
  // This role is deletable
  ROLE_CAPABILITIES_DELETE = 1;
  // This role's permissions can be changed
  ROLE_CAPABILITIES_UPDATE_PERMISSION_RULES = 2;
  // This role can be assigned to a user
  ROLE_CAPABILITIES_ASSIGN = 3;
}

///////////////////////////////////////////
// USERS
///////////////////////////////////////////

// UserStatus
enum UserStatus {
  // The user status is unspecified/unknown.
  USER_STATUS_UNSPECIFIED = 0;
  // The user is inactive and cannot sign in.
  USER_STATUS_INACTIVE = 1;
  // The user is active and can sign in.
  USER_STATUS_ACTIVE = 2;
}

// PasswordSetting - it is handy to have this as a separate message b/c
// otherwise we could not distinguish between empty string and an unspecified
// password.
message PasswordSetting {
  // The password
  string password = 1;
}

// EmailSetting - it is handy to have this as a separate message b/c otherwise
// we could not distinguish between empty string and an unspecified email.
message EmailSetting {
  // The email adress
  string email = 1;
}

// EntitySetting - it is handy to have this as a separate message b/c otherwise
// we could not distinguish between empty string and an unspecified entity.
message EntitySetting {
  // The entity which represents this user (e.g. a Person or Device Record).
  string entity_id = 1;
}

// User
message User {
  // Indicates whether the user is active. Only active users can sign in and
  // interact with the CaosDB Server.
  UserStatus status = 1;
  // Realm of this user. The realm is the authority which can authenticate this
  // user, e.g. 'PAM' when the user is a (POSIX) user from the server's host, or
  // 'CaosDB' when CaosDB server itself can authenticate the user.
  string realm = 2;
  // Name of this user.
  string name = 3;
  // Email setting of this user.
  EmailSetting email_setting = 4;
  // Entity setting of this user.
  EntitySetting entity_setting = 5;
  // List of roles of this user.
  repeated string roles = 6;
}

// Request message for the ListUsers RPC.
message ListUsersRequest {
  // Desired paging settings for the response.
  PagingRequest paging = 1;
}

// Response message for the ListUsers RPC.
message ListUsersResponse {
  // Actual paging setting of the response.
  PagingResponse paging = 1;
  // A subset (when paging is used) or the complete set (otherwise) of known
  // users.
  repeated User users = 2;
}

// Request message for the CreateSingleUser RPC
message CreateSingleUserRequest {
  // The new user.
  User user = 1;
  // The new password.
  PasswordSetting password_setting = 2;
}

// Response message for the CreateSingleUser RPC
message CreateSingleUserResponse {}

// Request message for the RetrieveSingleUser RPC
message RetrieveSingleUserRequest {
  // The user's realm
  string realm = 1;
  // The user's name
  string name = 2;
}

// Response message for the RetrieveSingleUser RPC
message RetrieveSingleUserResponse {
  // The user
  User user = 1;
  // The permissions of the user of the current session.
  repeated UserPermissions permissions = 2;
  // What can be done with this user
  repeated UserCapabilities capabilities = 3;
}

// Request message for the UpdateSingleUser RPC
message UpdateSingleUserRequest {
  // The new user.
  User user = 1;
  // The new password. Password will not be updated when this is unset.
  PasswordSetting password_setting = 2;
}

// Response message for the UpdateSingleUser RPC
message UpdateSingleUserResponse {}

// Request message for the DeleteSingleUser RPC
message DeleteSingleUserRequest {
  // The user's realm
  string realm = 1;
  // The user's name
  string name = 2;
}

// Response message for the DeleteSingleUser RPC
message DeleteSingleUserResponse {}

// Permissions for updating and deleting a user.
enum UserPermissions {
  // Unspecified permission
  USER_PERMISSIONS_UNSPECIFIED = 0;
  // The user can be deleted.
  USER_PERMISSIONS_DELETE = 1;
  // The password of the user can be updated.
  USER_PERMISSIONS_UPDATE_PASSWORD = 2;
  // The email of the user can be updated.
  USER_PERMISSIONS_UPDATE_EMAIL = 3;
  // The status (active/inactive) of the user can be changed.
  USER_PERMISSIONS_UPDATE_STATUS = 4;
  // The roles of the user can be altered.
  USER_PERMISSIONS_UPDATE_ROLES = 5;
  // Ths entity of the user can be set.
  USER_PERMISSIONS_UPDATE_ENTITY = 6;
}

// What can be done with a user (generally speaking, if permissions suffice).
enum UserCapabilities {
  // Unspecified capability
  USER_CAPABILITIES_UNSPECIFIED = 0;
  // User can be deleted.
  USER_CAPABILITIES_DELETE = 1;
  // The user's password can be updated.
  USER_CAPABILITIES_UPDATE_PASSWORD = 2;
}

///////////////////////////////////////////
// SERVICE DEFINITION
///////////////////////////////////////////

// A service for managing the access to the CaosDB Server via user accounts,
// roles and permissions.
service AccessControlManagementService {
  // Request the list of known users.
  rpc ListUsers(ListUsersRequest) returns (ListUsersResponse) {};

  // Retrieve a single user
  rpc RetrieveSingleUser(RetrieveSingleUserRequest)
      returns (RetrieveSingleUserResponse) {};

  // Create a single new user
  rpc CreateSingleUser(CreateSingleUserRequest)
      returns (CreateSingleUserResponse) {};

  // Update a single user
  rpc UpdateSingleUser(UpdateSingleUserRequest)
      returns (UpdateSingleUserResponse) {};

  // Delete a single user
  rpc DeleteSingleUser(DeleteSingleUserRequest)
      returns (DeleteSingleUserResponse) {};

  // Request the list of roles.
  rpc ListRoles(ListRolesRequest) returns (ListRolesResponse) {};

  // Retrieve a single role
  rpc RetrieveSingleRole(RetrieveSingleRoleRequest)
      returns (RetrieveSingleRoleResponse) {};

  // Create a single new role
  rpc CreateSingleRole(CreateSingleRoleRequest)
      returns (CreateSingleRoleResponse) {};

  // Update a single role
  rpc UpdateSingleRole(UpdateSingleRoleRequest)
      returns (UpdateSingleRoleResponse) {};

  // Delete a single role
  rpc DeleteSingleRole(DeleteSingleRoleRequest)
      returns (DeleteSingleRoleResponse) {};

  // List all known permissions
  rpc ListKnownPermissions(ListKnownPermissionsRequest)
      returns (ListKnownPermissionsResponse) {};
}
