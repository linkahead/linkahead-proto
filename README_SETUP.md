# How to Develop the CaosDB GRPC API

Make yourself familiar with
[protobuf](https://developers.google.com/protocol-buffers/) and
[grpc](https://grpc.io/).

## Formatting

We use
[clang-format-11](https://releases.llvm.org/11.0.0/tools/clang/docs/ClangFormat.html)
or later for formatting of `*.proto` files. Run `clang-format -i $(find proto/
-type f -iname "*.proto")` to format all the files. The `.clang-format`
contains all necessary configuration.

## Linting

We use [buf](https://docs.buf.build/) for linting. Run `buf lint`. The
`buf.yaml` contains all necessary configuration.

## Docs

We use [cmake](https://cmake.org) as build tool for the documenation and
generate the documentation with Sphinx and the Read-The-Docs theme.

See [DEPENDENCIES.md](DEPENDENCIES.md).

1. `cmake -B build`
2. `cd build`
3. `cmake --build . --target doc`

Results are in `build/doc_out/index.html`.

### Dependencies

* cmake >= v3.13
* protoc >= v3.12
* all the pypi packages from `doc/requirements.txt`.
