//
// This file is a part of the CaosDB Project.
//
// Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
// Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

// This is the main file of the caosdb.info.v1 package.
syntax                     = "proto3";
option java_multiple_files = true;
option java_package        = "org.caosdb.api.info.v1";

package caosdb.info.v1;

/////////////////////////////// SERVER VERSION

// The version of an agent (following SemVer 2.0.0)
message VersionInfo {
  // Major version number
  int32 major = 1;
  // Minor version number
  int32 minor = 2;
  // Patch version
  int32 patch = 3;
  // Pre-release version
  string pre_release = 4;
  // Build metadata
  string build = 5;
}

// Request message for the GetVersionInfo RPC
message GetVersionInfoRequest {}

// Response message for the GetVersionInfo RPC
message GetVersionInfoResponse {
  // The version of the server
  VersionInfo version_info = 1;
}

////////////////////////// SESSION

// Request message for the GetSessionInfo RPC
message GetSessionInfoRequest {}

// Response message for the GetSessionInfo RPC
message GetSessionInfoResponse {
  // The realm of the user who owns this session
  string realm = 1;
  // The name of the user who owns this session
  string username = 2;
  // The roles of the user who owns this session
  repeated string roles = 3;
  // Seconds until this sessions expires (approximately).
  int64 expires = 4;
  // The permissions of the user who owns this sessions
  repeated string permissions = 5;
}

// A service which provides general information about the server.
service GeneralInfoService {
  // Request the detailed version information from the server.
  rpc GetVersionInfo(GetVersionInfoRequest) returns (GetVersionInfoResponse) {};
  // Request information about the current session (user, roles, timeout...).
  rpc GetSessionInfo(GetSessionInfoRequest) returns (GetSessionInfoResponse) {};
}
