#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
# Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

find_program(SPHINX_CMD sphinx-build)
find_program(PROTOC_CMD protoc)
find_program(PROTOC_GEN_DOC_PLUGIN protoc-gen-doc)
if (NOT PROTOC_CMD)
    message(FATAL_ERROR "Protoc compiler  needs to be installed to generate the documentation")
elseif (NOT PROTOC_GEN_DOC_PLUGIN)
    message(FATAL_ERROR "Proto-gen-doc plugin needs to be installed to generate the documentation")
elseif (NOT SPHINX_CMD)
    message(FATAL_ERROR "Sphinx needs to be installed to generate the documentation")
else ()
    # Copy sources to build directory
    add_custom_target(doc-copy-sources
        COMMAND ${CMAKE_COMMAND} -E copy_directory
                ${CMAKE_CURRENT_SOURCE_DIR}
                ${CMAKE_CURRENT_BINARY_DIR}_build
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Copying ${CMAKE_CURRENT_SOURCE_DIR} sources to ${CMAKE_CURRENT_BINARY_DIR}_build")


    # PROTOC-GEN-DOC STUFF
    list(GET CAOSDB_API_PACKAGES 0 PACKAGE_NAME)
    string(REPLACE "." "/" PACKAGE_DIR ${PACKAGE_NAME})
    add_custom_target(doc-scalar-value-types
        COMMAND ${PROTOC_CMD}
        --doc_out=${CMAKE_CURRENT_BINARY_DIR}_build/
        --doc_opt=${CMAKE_CURRENT_SOURCE_DIR}/scalar-value-types.rst.tmpl,scalar-value-types.rst
            ${PACKAGE_DIR}/main.proto
        WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/proto/
        COMMENT "Generating API documentation for protobuf's native scalar value types."
        VERBATIM )

    # create rst pages for every package
    list(LENGTH CAOSDB_API_PACKAGES len_packages)
    math(EXPR len_packages "${len_packages} - 1")
    foreach (i RANGE "${len_packages}")
        list(GET CAOSDB_API_PACKAGES ${i} PACKAGE_NAME)

        configure_file(protoc-gen-doc.rst.in
            ${CMAKE_CURRENT_BINARY_DIR}_build/packages/${PACKAGE_NAME}.rst.tmpl)
        string(REPLACE "." "/" PACKAGE_DIR ${PACKAGE_NAME})

        list(APPEND doc-depends "doc-package-${PACKAGE_NAME}")
        file(GLOB_RECURSE PACKAGE_FILES
            RELATIVE ${PROJECT_SOURCE_DIR}/proto ${PROJECT_SOURCE_DIR}/proto/${PACKAGE_DIR}/*.proto)

        add_custom_target(doc-package-${PACKAGE_NAME}
            COMMAND ${PROTOC_CMD}
            --doc_out=${CMAKE_CURRENT_BINARY_DIR}_build/packages/
            --doc_opt=${CMAKE_CURRENT_BINARY_DIR}_build/packages/${PACKAGE_NAME}.rst.tmpl,${PACKAGE_NAME}.rst
                ${PACKAGE_FILES}
            WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/proto/
            COMMENT "Generating API documentation sources with the protoc-gen-doc plugin."

            VERBATIM )
    endforeach ()


    # SPHINX STUFF
    configure_file(conf.py.in conf.py)

    file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}_out)
    add_custom_target(doc
        COMMAND ${SPHINX_CMD}
            -b html
            -n
            -c ${CMAKE_CURRENT_BINARY_DIR}
            ${CMAKE_CURRENT_BINARY_DIR}_build
            ${CMAKE_CURRENT_BINARY_DIR}_out
        DEPENDS doc-copy-sources doc-scalar-value-types ${doc-depends}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Building API documentation with Sphinx."
        VERBATIM )
endif ()
