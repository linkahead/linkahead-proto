# caosdb-proto

## Welcome

This is the development repository of **caosdb-proto** - a collection of
protobuf files which make up the gRPC API of CaosDB. This repository is a part
of the CaosDB project.

## Setup

Please read the [README_SETUP.md](README_SETUP.md) for instructions on how to
develop, build and use this code.

## Further Reading

Please refer to the [official documentation](https://docs.indiscale.com/caosdb-proto/) for more information.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md).

## License

* Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>

All files in this repository are licensed under a [GNU Affero General Public
License](LICENCE.md) (version 3 or later).
